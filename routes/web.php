<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [App\Http\Controllers\HomePageController::class,'index'])->name('home'); 

Route::get('about', [App\Http\Controllers\HomePageController::class,'about'])->name('about');

Route::get('portofolio', [App\Http\Controllers\HomePageController::class,'portofolio'])->name('portfolio');

Route::get('contact',[App\Http\Controllers\HomePageController::class,'contact'])->name('contact');

