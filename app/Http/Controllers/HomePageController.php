<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomePageController extends Controller
{
    public function index (){

        return view('landing.page.home.home',['page'=>1]);
    }

    public function about (){

        return view('landing.page.about.about',['page'=>2] );
    }

    public function portofolio (){
        
        return view('landing.page.portofolio.portofolio',['page'=>3] );
    }

    public function contact (){
        
        return view('landing.page.contact.contact',['page'=>4] );
    }
}
